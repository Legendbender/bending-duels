package bd.ckateptb.duel.main;

import bd.ckateptb.duel.api.ActionBarAPI;
import bd.ckateptb.duel.api.CLUpdate;
import bd.ckateptb.duel.commands.DuelAdminExecutor;
import bd.ckateptb.duel.commands.DuelExecutor;
import bd.ckateptb.duel.events.*;
import bd.ckateptb.duel.menus.AcceptMenu;
import bd.ckateptb.duel.menus.SendMenu;
import bd.ckateptb.duel.mysql.MySql;
import bd.ckateptb.duel.threads.CheckDuelQueueThread;
import bd.ckateptb.duel.threads.RequestTimeoutThread;
import bd.ckateptb.duel.util.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class DuelMe extends JavaPlugin {

    /**
     * duelmanager class
     */
    private DuelManager duelManager;

    /**
     * filemanager class
     */
    private FileManager fileManager;

    /**
     * item manager class
     */
    private ItemManager itemManager;

    /**
     * message manager class
     */
    private MessageManager messageManager;

    private KitManager kitManager;

    /**
     * mysql class
     */
    private MySql mySql;

    /**
     * string to hold the plugin version
     */
    private static String version;

    private int errorCount;

    private AcceptMenu acceptMenu;
    private SendMenu sendMenu;

    public static String prefix;

    public DuelMe() {
        this.errorCount = 0;
    }


    @Override
    public void onEnable() {
        SendConsoleMessage.info("Enabling.");
        version = this.getDescription().getVersion();
        this.fileManager = new FileManager(this);
        this.setupYMLs();
        prefix = getFileManager().getPrefix();
        this.setupDependencies();
        this.messageManager = new MessageManager(this);
        this.duelManager = new DuelManager(this);
        this.itemManager = new ItemManager(this);
        this.kitManager = new KitManager(this);
        this.mySql = new MySql(this);
        getCommand("duel").setExecutor(new DuelExecutor(this));
        getCommand("dueladmin").setExecutor(new DuelAdminExecutor(this));
        CLUpdate clUpdate = new CLUpdate(this);
        Server server = getServer();
        ActionBarAPI.nmsver = Bukkit.getServer().getClass().getPackage().getName();
        ActionBarAPI.nmsver = ActionBarAPI.nmsver.substring(ActionBarAPI.nmsver.lastIndexOf(".") + 1);

        if (ActionBarAPI.nmsver.equalsIgnoreCase("v1_8_R1") || ActionBarAPI.nmsver.startsWith("v1_7_")) { // Not sure if 1_7 works for the protocol hack?
        	ActionBarAPI.useOldMethods = true;
        }
        Bukkit.getPluginManager().registerEvents(clUpdate, this);
        this.getFileManager().loadDuelArenas();
        //this.getFileManager().loadKits();TODO re-enable kits loading kits once tested fully
        if(this.setupTitleActionBar()) {
            SendConsoleMessage.info("NMS Version setup complete");
        } else {
            SendConsoleMessage.severe("Error setting up NMS related needed classes! Please make sure you are using the correct version compatible with this plugin! Plugin DISABLED");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        this.checkErrors();
        if (errorCount != 0) {
            return;
        }
        this.registerEvents();
        this.startTasks();
    }

    private boolean setupTitleActionBar() {
        String packageName = this.getServer().getClass().getPackage().getName();
        // Get full package string of CraftServer.
        // org.bukkit.craftbukkit.version
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);
        SendConsoleMessage.info("Server NMS Version: " + version);
        // Get the last element of the package

        try {
            final Class<?> clazz = Class.forName("bd.ckateptb.duel.nms.v1_12_R1.NMSHandler");
            // Check if we have a NMSHandler class at that location.
        } catch (final Exception e) {
            if(isDebugEnabled()) {
                SendConsoleMessage.warning("Error setting up NMS Class. " + e.getMessage());
            }
            return false;
        }
        SendConsoleMessage.info("Loading support for " + version);
        return true;
    }

    private void startTasks() {
        this.getServer().getScheduler().runTaskTimer(this, new RequestTimeoutThread(this), 20L, 120L);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new CheckDuelQueueThread(this), 30*20L, 30*20L);
    }

    /**
     * register the event classes
     */
    private void registerEvents() {
        new PlayerEvents(this);
        new PlayerKick(this);
        new SignEdit(this);
        new PlayerDeath(this);
        new PlayerRespawn(this);
        this.acceptMenu = new AcceptMenu(this);
        this.sendMenu = new SendMenu(this);
        new PlayerEvents(this);
        new SignEdit(this);
        new PlayerInteract(this);
    }

    @Override
    public void onDisable() {
        SendConsoleMessage.info("Disabling.");
        this.endAllRunningDuels();
        this.getFileManager().saveDuelArenas();
    }

    /**
     * end all the currently running duels
     */
    private void endAllRunningDuels() {
        DuelManager dm = this.getDuelManager();
        if (dm.getDuelArenas().size() == 0) {//if there are no duel arenas
            return;
        }
        for (DuelArena duelArena : dm.getDuelArenas()) {
            dm.endDuel(duelArena);
        }

    }

    /**
     * Check for startup errors for outdated plugin configs
     */
    private void checkErrors() {
        this.checkConfigVersions();
        if (this.errorCount != 0) {
            SendConsoleMessage.warning(ChatColor.RED + "There were " + ChatColor.AQUA + errorCount +
                    ChatColor.RED + " startup error(s), plugin DISABLED!");
            this.getPluginLoader().disablePlugin(this);
            return;
        } else {
            SendConsoleMessage.info("Successfully Enabled!");
        }
    }

    /**
     * setup the yml files used in the plugin
     */
    public void setupYMLs() {
        if (!(new File(getDataFolder(), "config.yml")).exists()) {
            SendConsoleMessage.info("Saving default config.yml.");
            saveDefaultConfig();
        }
        if (!(new File(getDataFolder(), "duelarenas.yml")).exists()) {
            SendConsoleMessage.info("Saving default duelarenas.yml.");
            this.getFileManager().saveDefaultDuelArenas();
        }

        if (!(new File(getDataFolder(), "messages.yml")).exists()) {
            SendConsoleMessage.info("Saving default messages.yml.");
            this.getFileManager().saveDefaultMessages();
        }

        if (!(new File(getDataFolder(), "signs.yml")).exists()) {
            SendConsoleMessage.info("Saving default signs.yml.");
            this.getFileManager().saveDefaultSigns();
        }

        /*if (!(new File(getDataFolder(), "kits.yml")).exists()) {//TODO re-enable kits config file creation once tested
            SendConsoleMessage.info("Saving default kits.yml.");
            this.getFileManager().saveDefaultKits();
        }*/
    }

    /**
     * check the config file versions and see
     * do they match the latest version
     */
    public void checkConfigVersions() {
        if (new File(getDataFolder(), "config.yml").exists()) {
            if (fileManager.getConfigVersion() != 1.8) {
                SendConsoleMessage.warning("Your config.yml is out of date! please remove or back it up before using the plugin!");
                errorCount++;
            }
        }

        if (new File(getDataFolder(), "messages.yml").exists()) {
            if (fileManager.getMessagesConfigVersion() != 1.3) {
                SendConsoleMessage.warning("Your messages.yml is out of date! please remove or back it up before using the plugin!");
                errorCount++;
            }
        }
    }

    /**
     * sets up the plugin main dependencies such as WorldEdit
     * disables the plugin if the required dependency is not present
     */
    private void setupDependencies() {
        if (this.getServer().getPluginManager().getPlugin("WorldEdit") != null) {
            SendConsoleMessage.info("WorldEdit found!");
        } else {
            SendConsoleMessage.warning("WorldEdit dependency not found, plugin disabled!");
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    /**
     * get the duel manager object
     *
     * @return duel manager object
     */
    public DuelManager getDuelManager() {
        return this.duelManager;
    }

    /**
     * get the file manager object
     *
     * @return file manager object
     */
    public FileManager getFileManager() {
        return this.fileManager;
    }

    /**
     * get the item manager object
     *
     * @return item manager object
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * get the plugin version
     *
     * @return the plugin version
     */
    public static String getVersion() {
        return version;
    }

    /**
     * get MySql object
     *
     * @return the MySql object
     */
    public MySql getMySql() {
        return this.mySql;
    }

    /**
     * is debug mode enabled
     *
     * @return true if enabled, false if not
     */
    public boolean isDebugEnabled() {
        return getFileManager().isDebugEnabled();
    }

    public boolean isUsingSeperatedInventories() {
        return this.getFileManager().isUsingSeperateInventories();
    }

    public static String getPrefix() {
        return prefix;
    }

    public AcceptMenu getAcceptMenu() {
        return acceptMenu;
    }
    public SendMenu getSendMenu() {
        return sendMenu;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }

    public KitManager getKitManager() {
        return kitManager;
    }
}