package bd.ckateptb.duel.api;

import org.bukkit.entity.Player;

public class TitleActionbar {
	
	public static void sendTitle(Player p1, Player p2, String s1, String s2, Integer i1, Integer i2, Integer i3) {
		p1.sendTitle(s1, "", i1, i2, i3);
		p2.sendTitle(s1, "", i1, i2, i3);
	}

	public static void sendActionbar(Player p1, Player p2, String s) {
		ActionBarAPI.sendActionBar(p1,s);
		ActionBarAPI.sendActionBar(p2,s);
	}
}
