package bd.ckateptb.duel.threads;

import bd.ckateptb.duel.api.TitleActionbar;
import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.*;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class DuelStartThread extends BukkitRunnable {

    private DuelMe plugin;
    private Player sender;
    private Player target;
    private DuelArena duelArena;
    private int countDown;

    public DuelStartThread(DuelMe plugin, Player sender, Player target, DuelArena duelArena) {
        this.plugin = plugin;
        this.sender = sender;
        this.target = target;
        this.countDown = plugin.getFileManager().getDuelCountdownTime();
        this.duelArena = duelArena;
    }

    @Override
    public void run() {
        DuelManager dm = plugin.getDuelManager();
        FileManager fm = plugin.getFileManager();
        MessageManager mm = plugin.getMessageManager();
        int duelTime = fm.getDuelTime();
        String senderName = sender.getName();
        String targetName = target.getName();
        UUID senderUUID = sender.getUniqueId();
        UUID targetUUID = target.getUniqueId();
        int duelSize = duelArena.getPlayers().size();

        if (duelSize == 0) {
            dm.endDuel(duelArena);
            this.cancel();
        }


        if (this.countDown > 0 && duelSize == 2) {
            String duelStartTitle = mm.getDuelStartingTitleMessage();
            duelStartTitle = duelStartTitle.replaceAll("%seconds%", String.valueOf(this.countDown));

            String duelStartSubtitle = mm.getDuelStartingSubtitleMessage();
            duelStartSubtitle = duelStartSubtitle.replaceAll("%seconds%", String.valueOf(this.countDown));

            TitleActionbar.sendTitle(sender, target, duelStartTitle, duelStartSubtitle, 10, 10, 10);
            this.countDown--;
        } else {
            if(duelSize == 2) {
                TitleActionbar.sendTitle(sender, target, mm.getDuelStartedMessage(), "", 10, 10, 10);
                sender.setFoodLevel(20);
                sender.setHealth(sender.getMaxHealth());
                target.setFoodLevel(20);
                target.setHealth(target.getMaxHealth());
                duelArena.setDuelState(DuelState.STARTED);
                dm.surroundLocation(duelArena.getSpawnpoint1(), Material.AIR);
                dm.surroundLocation(duelArena.getSpawnpoint2(), Material.AIR);
                dm.updateDuelStatusSign(duelArena);
            }

            //dm.removeFrozenPlayer(senderUUID);
            //dm.removeFrozenPlayer(targetUUID);

            if (plugin.isDebugEnabled()) {
                SendConsoleMessage.debug("Stopping duel start thread.");
            }
            this.cancel();

            if (duelTime != 0 && duelSize == 2) {
                if (plugin.isDebugEnabled()) {
                    SendConsoleMessage.debug("Duel time limit is set, starting countdown task.");
                }
                new DuelTimeThread(plugin, sender, target, duelArena, duelTime).runTaskTimer(plugin, 20L, 20L);
            }
        }
    }
}
