package bd.ckateptb.duel.threads;

import bd.ckateptb.duel.api.TitleActionbar;
import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelArena;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.MessageManager;
import bd.ckateptb.duel.util.SendConsoleMessage;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Frank on 24/07/2014.
 */
public class DuelTimeThread extends BukkitRunnable {

    private DuelMe plugin;
    private Player sender;
    private Player target;
    private DuelArena duelArena;
    private int duelTime;

    public DuelTimeThread(DuelMe plugin, Player sender, Player target,DuelArena duelArena, int duelTime) {
        this.plugin = plugin;
        this.sender = sender;
        this.target = target;
        this.duelTime = duelTime;
        this.duelArena = duelArena;
    }

    @Override
    public void run() {
        DuelManager dm = plugin.getDuelManager();
        MessageManager mm = plugin.getMessageManager();
        int duelSize = duelArena.getPlayers().size();

        if(duelArena.getPlayers().size() == 1) {
            if(plugin.isDebugEnabled()) {
                SendConsoleMessage.debug("cancelling duel time thread as player size is now 1");
            }
            this.cancel();// just need to cancel if arena size gets to 1, start duel thread will handle the rewards.
        }

        if (this.duelTime > 0 && duelSize == 2) {
            //Util.setTime(sender, target, this.duelTime);
            String duelEndActionBar = mm.getDuelRemainingActionBarMessage();
            duelEndActionBar = duelEndActionBar.replaceAll("%seconds%", String.valueOf(this.duelTime));
            TitleActionbar.sendActionbar(sender, target, duelEndActionBar);
            this.duelTime--;
        } else {
            if(plugin.isDebugEnabled()) {
                SendConsoleMessage.debug("Ending duel time thread, time is up!");
            }
            if(duelSize == 2) {
                //Util.setTime(sender, target, this.duelTime);
            }

            dm.endDuel(duelArena);
            this.cancel();
        }
    }
}

