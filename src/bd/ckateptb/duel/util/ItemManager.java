package bd.ckateptb.duel.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import bd.ckateptb.duel.main.DuelMe;

import java.util.List;
import java.util.UUID;

public class ItemManager {

    private DuelMe plugin;

    public ItemManager(DuelMe plugin){
       this.plugin = plugin;
    }

    /**
     * performs the initial commands for what items players
     * get when a duel starts
     * @param player the player
     */
    public void givePlayerDuelItems(Player player){
        FileManager fm = plugin.getFileManager();
        List<String> commands = fm.getDuelStartCommands();
        for(String commandIn : commands){
            commandIn = commandIn.replaceAll("%player%", player.getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(),commandIn);
        }
    }

    /**
     * performs the winning player commands
     * @param player the player
     */
    public void giveWinningPlayerRewards(Player player, String losingPlayerName){
        FileManager fm = plugin.getFileManager();
        List<String> commands = fm.getDuelWinnerCommands();
        for(String commandIn : commands){
            commandIn = commandIn.replaceAll("%player%", player.getName());
            commandIn = commandIn.replaceAll("%losingplayer%", losingPlayerName);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(),commandIn);
        }
    }

    /**
     * reward a player
     * @param arena the arena which the player is in
     */
    public void rewardPlayer(DuelArena arena, String losingPlayerName) {
        DuelManager dm = plugin.getDuelManager();
        FileManager fm = plugin.getFileManager();
        MessageManager mm = plugin.getMessageManager();

        if(arena.getPlayers().size() == 1){ //if there is only one player left in the arena
        if(plugin.isDebugEnabled()){
            SendConsoleMessage.debug("1 player left, now rewarding.");
        }
            for(UUID playerIn: arena.getPlayers()){
                Player winningPlayer = Bukkit.getPlayer(playerIn);
                String winningPlayerName = winningPlayer.getName();
                if(winningPlayer != null){
                    dm.restorePlayerData(winningPlayer);
                    this.giveWinningPlayerRewards(winningPlayer, losingPlayerName);//give them a reward
                    Util.sendMsg(winningPlayer, mm.getDuelRewardMessage());
                }
            }

            dm.resetArena(arena);
        }

    }

}
