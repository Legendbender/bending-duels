package bd.ckateptb.duel.menus;

import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import bd.CKATEPTb.BDClans.menu.menu;
import bd.CKATEPTb.BDClans.storage.DBConnection;
import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.SendConsoleMessage;

public class SendMenu implements Listener{
	public static String onlineplayertitle() {
		return "&f&l��������|���� ������� �����?".replaceAll("&", "�");
	}
	
    private DuelMe plugin;
    private Inventory sendMenu;
    private String senderName;
    private ItemStack accept, ignore;

    public SendMenu(DuelMe plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    
	public Inventory openOnlinePlayerInventory (Player p,Integer page) throws IllegalArgumentException, SQLException {
		final Inventory inv = Bukkit.createInventory(p, 9 * 1, onlineplayertitle());
					if (p.isOnline()) {
						new BukkitRunnable() {
							@Override
							public void run() {
						        try {
									OnlinePlayerBady(p,inv,page);
								} catch (SQLException e) {
									e.printStackTrace();
								}
							}
						}.runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("DuelMe"));
					}
		return inv; 
	}
	
	public static void OnlinePlayerBady(Player p, Inventory inv, Integer page) throws SQLException {
		int online = Bukkit.getOnlinePlayers().size();
		if(online<10) {
			page = 1;
		}
		if(page==1) {
			int i=0;
			for(Player pl : Bukkit.getOnlinePlayers()) {
				inv.setItem(i, getPlayerStat(pl));
				i++;
				if(online>9&&i==8) {
					inv.setItem(i, menu.getPageNextItem());
					break;
				}
			}
		} else if(page==2) {
			int skipplayer = 0;
			int i=1;
			inv.setItem(0, menu.getPageBackItem());
			for(Player pl : Bukkit.getOnlinePlayers()) {
				if(skipplayer>7) {
					inv.setItem(i, getPlayerStat(pl));
					i++;
					if(online>8+(8*(page-1))&&i==8) {
						inv.setItem(i, menu.getPageNextItem());
						break;
					}
				} else
					skipplayer++;
			}
		} else if(page>2) {
			int skipplayer = 0;
			int i=1;
			inv.setItem(0, menu.getPageBackItem());
			for(Player pl : Bukkit.getOnlinePlayers()) {
				if(skipplayer>7+(7*(page-2))) {
					inv.setItem(i, getPlayerStat(pl));
					i++;
					if(online>8+(7*(page-1))&&i==8) {
						inv.setItem(i, menu.getPageNextItem());
						break;
					}
				} else
					skipplayer++;
			}
		}
	}
	
	public static ItemStack getPlayerStat(Player p) throws SQLException {
		ItemStack item = getHead(p);
		ItemMeta meta = item.getItemMeta();		
		ArrayList<String> Lore = new ArrayList<String>();
		meta.setDisplayName(p.getName());	
		Lore.clear();
		Lore.add(("&6&l����: "+DBConnection.getKills(p)).replaceAll("&", "�"));
		Lore.add(("&6&l����: "+DBConnection.getDeath(p)).replaceAll("&", "�"));
		Lore.add(("&6&l���������: "+DBConnection.getPlayerStat(p)).replaceAll("&", "�"));
		meta.setLore(Lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getHead(Player p) {
		ItemStack item = new ItemStack((Material.SKULL_ITEM), 1, (short) 3);
		SkullMeta sm = (SkullMeta) item.getItemMeta();
		sm.setOwningPlayer(p);
		item.setItemMeta(sm);
		item.toString();
		return item;
	}
	
    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent e) throws IllegalArgumentException, SQLException{
		if(e.getInventory().getName().contains(onlineplayertitle())) {
			e.setCancelled(true);
			ItemStack clicked = e.getCurrentItem();
			if(clicked!=null&&clicked.hasItemMeta()){
			Player player = (Player) e.getWhoClicked(); 
			ItemMeta meta = clicked.getItemMeta();
	        DuelManager dm = plugin.getDuelManager();
				if (meta.getDisplayName().equalsIgnoreCase(menu.getPageNextItem().getItemMeta().getDisplayName())) {
					if(!menu.getPlayerPage.containsKey(player)) {
						menu.getPlayerPage.put(player, 1);
					}
					int i = menu.getPlayerPage.get(player)+1;
					menu.getPlayerPage.remove(player);
					menu.getPlayerPage.put(player, i);
					player.openInventory(openOnlinePlayerInventory(player, i));
				}
				else if (meta.getDisplayName().equalsIgnoreCase(menu.getPageBackItem().getItemMeta().getDisplayName())) {
					if(!menu.getPlayerPage.containsKey(player)) {
						menu.getPlayerPage.put(player, 1);
					}
					int i = menu.getPlayerPage.get(player)-1;
					menu.getPlayerPage.remove(player);
					menu.getPlayerPage.put(player, i);
					player.openInventory(openOnlinePlayerInventory(player, i));
				} else if (clicked.getType()==Material.SKULL_ITEM) {
		            dm.sendDuelRequest(player, meta.getDisplayName(), null);
		            player.closeInventory();
				}
			}
		}
    }
}
