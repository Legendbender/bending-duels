package bd.ckateptb.duel.commands;

import bd.CKATEPTb.BDClans.menu.menu;
import bd.ckateptb.duel.commands.admin.AboutCmd;
import bd.ckateptb.duel.commands.duel.*;
import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.menus.SendMenu;
import bd.ckateptb.duel.util.Util;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DuelExecutor extends CmdExecutor implements CommandExecutor {

    public DuelExecutor(DuelMe plugin) {
        super(plugin);
        DuelCmd accept = new AcceptCmd(plugin, "duelme.player.accept");
        DuelCmd send = new SendCmd(plugin, "duelme.player.send");
        DuelCmd leave = new LeaveCmd(plugin, "duelme.player.leave");

        addCmd("accept", accept, new String[]{
                "a"
        });

        addCmd("send", send, new String[]{
                "s"
        });

        addCmd("leave", leave, new String[]{
                "l"
        });
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("duel")) {

            if (args.length < 1) {
                	if(sender instanceof Player) {
                		Player pl = (Player) sender;
                		try {
        					if(menu.getPlayerPage.containsKey(pl)) {
        						menu.getPlayerPage.remove(pl);
        					}
        					menu.getPlayerPage.put(pl, 1);
                			pl.openInventory(plugin.getSendMenu().openOnlinePlayerInventory(pl, menu.getPlayerPage.get(pl)));
						} catch (IllegalArgumentException | SQLException e) {
							e.printStackTrace();
						}
                    }
                    return true;

            }

            String sub = args[0].toLowerCase();

            DuelCmd cmd = (DuelCmd) super.getCmd(sub);

            if (cmd == null) {
                return true;
            }

            sub = cmd.getCommand(sub);

            if (sender instanceof Player) {
                Player p = (Player) sender;

                if (!p.hasPermission(cmd.permission)) {
                    Util.sendMsg(p, cmd.NO_PERM);
                    return true;
                }
            }

            try {
                cmd.run(sender, sub, makeParams(args, 1));
            } catch (ArrayIndexOutOfBoundsException e) {
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Util.sendMsg(sender, cmd.GEN_ERROR);
                return true;
            } catch (NoClassDefFoundError e) {
                return true;
            }

            return true;

        }

        return false;
    }


}

