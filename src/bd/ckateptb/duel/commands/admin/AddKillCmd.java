package bd.ckateptb.duel.commands.admin;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.mysql.FieldName;
import bd.ckateptb.duel.mysql.MySql;
import bd.ckateptb.duel.util.DuelArena;
import bd.ckateptb.duel.util.FileManager;
import bd.ckateptb.duel.util.SendConsoleMessage;
import bd.ckateptb.duel.util.Util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class AddKillCmd extends DuelAdminCmd {
    public AddKillCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(DuelArena duelArena, CommandSender sender, String subCmd, String[] args) {
        MySql mySql = plugin.getMySql();
        FileManager fm = plugin.getFileManager();

        if(args.length < 1 ) {
            Util.sendMsg(sender, "Usage /dueladmin addkill <playername>");
            return;
        }

        if(args.length == 1 ) {
            if(!fm.isMySqlEnabled()) {
                Util.sendMsg(sender, ChatColor.RED + "MySql is NOT enabled you cannot use this command.");
                return;
            }
            String playerNameIn = args[0];
            SendConsoleMessage.debug(playerNameIn);
            Player player = plugin.getServer().getPlayerExact(playerNameIn);
            UUID playerUUID = player.getUniqueId();

            if(player != null) {
                String playerName = player.getName();
                Util.sendMsg(sender, "Adding kill for player: " + ChatColor.AQUA + playerName);
                mySql.addPlayerKillDeath(playerUUID, playerName, FieldName.KILL);
            }
        }
    }
}
