package bd.ckateptb.duel.commands.admin;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.*;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ReloadCmd extends DuelAdminCmd {
    public ReloadCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(DuelArena duelArena, CommandSender sender, String subCmd, String[] args) {
        DuelManager dm = plugin.getDuelManager();
        FileManager fm = plugin.getFileManager();
        Util.sendMsg(sender, ChatColor.GREEN + "Reloading configs, please wait.");
        plugin.reloadConfig();
        for(DuelArena arena: dm.getDuelArenas()) {
            if(arena.getDuelState() == DuelState.STARTED ||
                    arena.getDuelState() == DuelState.STARTING) {
                dm.endDuel(arena);
                Util.sendMsg(sender, ChatColor.YELLOW + "Duel active in arena: " + arena.getName() + ", Ending.");
            }
        }
        Util.sendMsg(sender, ChatColor.YELLOW + "Reloaded config.yml!");
        Util.sendMsg(sender, ChatColor.YELLOW + "Saving Duel arenas!");
        fm.saveDuelArenas();
        Util.sendMsg(sender, ChatColor.YELLOW + "Clearing Duel arena cache!");
        dm.getDuelArenas().clear();
        Util.sendMsg(sender, ChatColor.YELLOW + "Reloading Duel arena config!");
        fm.reloadDuelArenas();
        Util.sendMsg(sender, ChatColor.YELLOW + "Loading Duel arenas from config!");
        fm.loadDuelArenas();
        Util.sendMsg(sender, ChatColor.YELLOW + "Reloading messages!");
        fm.reloadMessages();
        Util.sendMsg(sender, ChatColor.GREEN + "Complete!");
    }
}
